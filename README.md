# JHATEOAS

This library serves as the foundation of other media type libraries produced by its developer, such as [JHAL](https://gitlab.com/073TruantKernel/jhal).

`jhateoas` specifies a marker interface `DataTransferObject` to indicate that a class is a DTO that can be serialized along with additional metadata as a class that extends `HateoasEntity` and implements the `HateoasRepresentation` interface. The instantiation of a `HateoasRepresentation` is handled by a builder that implements the `Hateoas` interface.

Additionally this library specifies a `LinkRelation` interface that's implemented by `RegisteredLinkRelation`, which provides an enumeration of Link Relations [defined](https://www.iana.org/assignments/link-relations/link-relations.xhtml) by the IANA. 

An enumeration of HTTP Methods is provided by `HttpMethod`. 

A Hyperlink Object is provided by `Hyperlink` and is serialized by `HyperlinkSerializer`

`ResourceArchetype` provides an enumeration of [resource archetypes](http://uniknow.github.io/AgileDev/site/0.1.9-SNAPSHOT/parent/rest/resource-archetypes.html) used to describe the characteristics of an end-point. These can be declared on a resource through the use of the `Archetype` interface annotation.
