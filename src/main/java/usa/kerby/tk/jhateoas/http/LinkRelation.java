package usa.kerby.tk.jhateoas.http;

/**
 * @author Trevor Kerby
 * @since Jul 22, 2020
 */
public interface LinkRelation {

	public String toString();
	public String getName();

}
