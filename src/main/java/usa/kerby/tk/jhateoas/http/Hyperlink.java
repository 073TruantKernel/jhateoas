package usa.kerby.tk.jhateoas.http;

import java.net.URI;
import java.net.URISyntaxException;

import jakarta.json.bind.annotation.JsonbTypeSerializer;
import jakarta.ws.rs.core.UriBuilder;


/**
 * @author Trevor Kerby
 * @since Jun 18, 2020
 */

@JsonbTypeSerializer(HyperlinkSerializer.class)
public class Hyperlink {

	public static Hyperlink createPaginationHyperlink(URI uri, int offset, int limit) {
		try {
			return new Hyperlink.Builder(new URI(uri + "?offset=" + offset + "&limit=" + limit)).build();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static class Builder {
		private URI href;
		private String hrefTemplate;
		private String type;
		private String deprecation;
		private String name;
		private String profile;
		private String title;
		private String hreflang;

		/**
		 * @param href
		 */
		public Builder(URI href) {
			this.href = href;
		}

		public Builder(UriBuilder uriBuilder, Class<?> resource) {
			this.href = uriBuilder.clone().path(resource).build();
		}
		
		public Builder(UriBuilder uriBuilder, String path) {
			this.href = uriBuilder.clone().path(path).build();
		}

		public Builder(String template) throws URISyntaxException {
			this.hrefTemplate = template;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public Builder deprecation(String uri) {
			this.deprecation = uri;
			return this;
		}

		public Builder named(String name) {
			this.name = name;
			return this;
		}

		public Builder profiles(String profile) {
			this.profile = profile;
			return this;
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder hreflang(String lang) {
			this.hreflang = lang;
			return this;
		}

		public Hyperlink build() {
			return new Hyperlink(this);
		}

	}

	private final String href;
	private final Boolean templated;
	private final String type;
	private final String deprecation;
	private final String name;
	private final String profile;
	private final String title;
	private final String hreflang;

	public Hyperlink(Builder builder) {
		super();
		if (builder.hrefTemplate != null) {
			this.templated = true;
			this.href = builder.hrefTemplate;
		} else {
			this.templated = false;
			this.href = builder.href.toString();
		}
		this.type = builder.type;
		this.deprecation = builder.deprecation;
		this.name = builder.name;
		this.profile = builder.profile;
		this.title = builder.title;
		this.hreflang = builder.hreflang;
	}

	public Hyperlink(String href) {
		this.href = href;
		this.templated = false;
		this.type = null;
		this.deprecation = null;
		this.name = null;
		this.profile = null;
		this.title = null;
		this.hreflang = null;
	}
	
	public Hyperlink(UriBuilder uriBuilder, Class<?> resource) {
		this(uriBuilder.clone().path(resource).build().toString());
	}

	public String getHref() {
		return href;
	}

	public boolean isTemplated() {
		return templated;
	}

	public String getType() {
		return type;
	}

	public String getDeprecation() {
		return deprecation;
	}

	public String getName() {
		return name;
	}

	public String getProfile() {
		return profile;
	}

	public String getTitle() {
		return title;
	}

	public String getHreflang() {
		return hreflang;
	}

}
