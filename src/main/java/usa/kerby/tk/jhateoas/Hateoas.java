package usa.kerby.tk.jhateoas;

import java.net.URI;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public interface Hateoas {

	/**
	 * @return
	 */
	URI getUri();

}
