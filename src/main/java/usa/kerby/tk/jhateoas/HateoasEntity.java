package usa.kerby.tk.jhateoas;

import java.net.URI;

/**
 * @author Trevor Kerby
 * @since Aug 1, 2020
 */
public abstract class HateoasEntity implements HateoasRepresentation {

	private final URI self;

	public abstract static class Builder<T extends Builder<T>> implements Hateoas {

		protected URI uri;
		protected DataTransferObject[] dto;

		public Builder(URI self, DataTransferObject... dto) {
			this.uri = self;
			this.dto = dto;
		}

		public abstract HateoasRepresentation build();

		protected abstract T self();
	}

	protected HateoasEntity(Hateoas builder) {
		this.self = builder.getUri();
	}

	public URI getUri() {
		return self;
	}

}
